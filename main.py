from search import search
from xl import reader
from xl import writer


def main():
    search_results = {}
    firms = reader.get_firms()
    for firm in firms:
        result = search.search(firm)
        search_results[firm] = result
    writer.write(search_results)


main()
