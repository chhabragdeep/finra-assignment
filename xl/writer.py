from openpyxl import Workbook
from config import configuration as cfg
import json


def write(firms):
    wb = Workbook()
    ws = wb.create_sheet("FinraFirmSearchResults", 0)

    coll_firm = 'A1'
    coll_result_count = 'B1'
    coll_firm_data = 'C1'
    ws[coll_firm] = 'Firm Name'
    ws[coll_result_count] = '# of Results'
    ws[coll_firm_data] = 'Search Dump'

    inc = 2
    for firm, firm_data in firms.iteritems():
        coll_firm = 'A' + str(inc)
        coll_result_count = 'B' + str(inc)
        coll_firm_data = 'C' + str(inc)
        ws[coll_firm] = firm
        ws[coll_result_count] = firm_data['totalResults']
        ws[coll_firm_data] = json.dumps(firm_data)
        inc += 1

    wb.save(cfg['output_data_dir'] + '/' + cfg['output_file'])
