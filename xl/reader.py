from openpyxl import load_workbook
from config import configuration as cfg


def get_firms():
    wb = load_workbook(filename=cfg['input_data_dir'] + '/' + cfg['input_data_file'])
    sheet_ranges = wb['Sheet1']

    firms = []
    inc = 1
    coll = 'A' + str(inc)
    while sheet_ranges[coll].value is not None:
        firms.append(sheet_ranges[coll].value)
        inc += 1
        coll = 'A' + str(inc)

    return firms
