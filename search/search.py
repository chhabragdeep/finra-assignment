from config import configuration as cfg
import requests as req
import json

"""
Search...
"""


def search(query):
    base_url = cfg['search_base_url']
    headers = {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36'}

    payload = {'hl': 'true', 'nrows': 12, 'r': 25,
               'sort': 'score desc', 'wt': 'json', 'query': query}
    res = req.get(base_url, params=payload, headers=headers)
    json_response = json.loads(res.text)
    return json_response['results']['BROKER_CHECK_FIRM']