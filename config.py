import sys
import json
import logging

logger = logging.getLogger()

"""
Initialize configuration. It reads the config file from config/config.json
"""

configuration = None
with open('config/config.json', 'r') as cfg_file:
    configuration = json.load(cfg_file)

if configuration is None:
    logger.fatal("CONFIG FILE NOT FOUND. Exiting...")
    sys.exit(1)
