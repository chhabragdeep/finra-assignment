# finra-assignment



## Contents:
* Setup Guide
* Running Guide
* Whats what

## Setup Guide
Follow the below steps to setup

* git clone
* cd to cloned dir
* create python virtual env 
```
		virtualenv venv
```
* activate venv
```
		. venv/bin/activate
```
* Install dependencies
```
		pip install -r requirements.txt
```

## Running Guide
Use the following to run 

```
		#make sure virtualenv is activated
		python main.py  
```

## Whats what
* main.py is the entry point to the tool
* config/config.json has the application configuration - input data dir + filename with firm names and output data dir + output filename.
* final output in generated in "data/output/search_out.xlsx" (and always overwritten on each run of the tool)